package service.finance.util;

public class Queries {
    public static final String FETCH_ALL_BILLS = """
            select b.id,
                   b.complete,
                   b.totalprice,
                   b.customerid,
                   pc.fullname,
                   b.createdat
            from bill b
                     left join profile.customer pc on pc.id = b.customerid;
            """;

    public static final String FETCH_ALL_BILL_ITEMS = """
            select ii.id,
                   ii.name,
                   ii.quantity,
                   ii.price,
                   bi.quantity as selectedQuantity,
                   bi.billid,
                   bi.exempt
            from billitem bi
                     left join inventory.inventory ii on ii.id = bi.inventoryid
            where bi.billid = :billId
              and bi.exempt is false
            """;
}
