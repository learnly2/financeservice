package service.finance.util;

import java.util.HashMap;

public class QueueDeclaration {
    public static final String PH_EXCHANGE = "PH_EXCHANGE";
    public static final String PH_DEAD_LETTER_EXCHANGE = "PH_DEAD_LETTER_EXCHANGE";
    public static final String PH_DEAD_LETTER_QUEUE = "PH_DEAD_LETTER_QUEUE";


    public static HashMap<String, String> queueMap() {
        HashMap<String, String> q = new HashMap<>();

        q.put("QUANTITY_QUEUE_ROUTING_KEY", "QUANTITY_QUEUE");
        return q;
    }
}
