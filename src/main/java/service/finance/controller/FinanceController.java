package service.finance.controller;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import service.finance.exception.ConflictException;
import service.finance.model.Bill;
import service.finance.model.BillItem;
import service.finance.payload.request.BillItemRequest;
import service.finance.payload.request.BillRequest;
import service.finance.payload.request.CompleteBillRequest;
import service.finance.payload.request.ExemptBillItemRequest;
import service.finance.payload.response.CreateBillResponse;
import service.finance.projection.BillItemExtendedProjection;
import service.finance.projection.BillProjection;
import service.finance.publisher.RabbitMQPublisher;
import service.finance.service.BillItemServiceImpl;
import service.finance.service.BillServiceImpl;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/finance", produces = {"application/json; charset=utf-8"}, consumes = {"application/json"})
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class FinanceController {

    final BillServiceImpl billService;
    final BillItemServiceImpl billItemService;
    final RabbitMQPublisher rabbitMQPublisher;

    public FinanceController(
            BillServiceImpl billService,
            BillItemServiceImpl billItemService,
            RabbitMQPublisher rabbitMQPublisher
    ) {
        this.billService = billService;
        this.billItemService = billItemService;
        this.rabbitMQPublisher = rabbitMQPublisher;
    }

    @Operation(summary = "Create bill")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/bill")
    @Transactional(rollbackFor = Exception.class)
    public Mono<CreateBillResponse> createBill(@Valid @RequestBody BillRequest request) {
        return billService.save(request.billId(), request.totalPrice(), request.customerId())
                .flatMap(bi -> addBillItems(request.items(), bi.getId())
                        .collectList()
                        .map(items -> new CreateBillResponse(bi, items))
                );
    }

    Flux<BillItem> addBillItems(List<BillItemRequest> items, String billId) {
        return Flux.fromIterable(items)
                .flatMap(item -> billItemService.save(item.quantity(), item.price(), item.inventoryId(), billId));
    }

    @Operation(summary = "Complete bill")
    @PutMapping("/bill")
    public Mono<Bill> completeBill(@Valid @RequestBody CompleteBillRequest request) {
        return billService
                .checkIfComplete(request.billId())
                .filter(b -> !b)
                .switchIfEmpty(Mono.error(new ConflictException("CBE-001")))
                .flatMap(b -> billService.completeBill(request.billId())
                        .flatMap(bi -> billItemService.getAllByBill(bi.getId())
                                .collectList()
                                .doOnNext(rabbitMQPublisher::sendQuantityMessage)
                                .thenReturn(bi)
                        )
                );
    }

    @Operation(summary = "Exempt bill item")
    @PutMapping("/bill/item")
    public Mono<BillItem> exemptBillItem(@Valid @RequestBody ExemptBillItemRequest request) {
        return billService
                .checkIfComplete(request.billId())
                .filter(isComplete -> !isComplete)
                .switchIfEmpty(Mono.error(new ConflictException("CBE-002")))
                .flatMap(bill -> billItemService.exemptItem(request.billId(), request.inventoryId()))
                .flatMap(billItem -> billService
                        .reducePrice(request.billId(), billItem.getPrice())
                        .thenReturn(billItem)
                );
    }

    //TODO paginate & fetch by complete status
    @Operation(summary = "Exempt bill item")
    @GetMapping("/bills")
    public Flux<BillProjection> allBills() {
        return billService.getAllBills();
    }

    @Operation(summary = "Fetch bill items")
    @GetMapping("/bill/items")
    public Flux<BillItemExtendedProjection> getAllBillItems(@RequestParam(name = "bId") String billId) {
        return billItemService.getAllBillItems(billId);
    }
}
