package service.finance.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import service.finance.model.audit.DateAudit;

import java.math.BigDecimal;

@Table("bill")
public class Bill extends DateAudit {
    @Id
    @Column("id")
    private String id;

    @Column("totalprice")
    private BigDecimal totalPrice;

    @Column("complete")
    private boolean complete = false;

    @Column("customerid")
    private String customerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
