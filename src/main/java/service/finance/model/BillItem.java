package service.finance.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import service.finance.model.audit.DateAudit;

import java.math.BigDecimal;

@Table("billitem")
public class BillItem extends DateAudit {

    @Id
    @Column("id")
    private String id;

    @Column("quantity")
    private int quantity;

    @Column("price")
    private BigDecimal price;

    @Column("exempt")
    private boolean exempt = false;

    @Column("billid")
    private String billId;

    @Column("inventoryid")
    private String inventoryId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isExempt() {
        return exempt;
    }

    public void setExempt(boolean exempt) {
        this.exempt = exempt;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }
}
