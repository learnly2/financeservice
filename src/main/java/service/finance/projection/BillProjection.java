package service.finance.projection;

import org.springframework.data.relational.core.mapping.Column;

import java.math.BigDecimal;
import java.time.Instant;

public record BillProjection(
        @Column("id") String id,
        @Column("complete") boolean complete,
        @Column("totalprice") BigDecimal totalPrice,
        @Column("customerid") String customerId,
        @Column("fullname") String fullName,
        @Column("createdat") Instant createdAt
) {
}
