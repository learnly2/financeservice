package service.finance.projection;

import org.springframework.data.relational.core.mapping.Column;

public record BillItemProjection(
        @Column("inventoryid") String inventoryId,
        @Column("quantity") int quantity,
        @Column("billid") String billId,
        @Column("exempt") boolean exempt
) {
}
