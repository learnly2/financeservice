package service.finance.projection;

import org.springframework.data.relational.core.mapping.Column;

import java.math.BigDecimal;

public record BillItemExtendedProjection(
        @Column("id") String id,
        @Column("name") String name,
        @Column("quantity") int quantity,
        @Column("price") BigDecimal price,
        @Column("selectedQuantity") int selectedQuantity,
        @Column("billid") String billId,
        @Column("exempt") boolean exempt
) {
}
