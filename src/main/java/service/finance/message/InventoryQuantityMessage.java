package service.finance.message;

import service.finance.projection.BillItemProjection;

import java.util.List;

public record InventoryQuantityMessage(List<BillItemProjection> items) {
}
