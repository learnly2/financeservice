package service.finance.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.stereotype.Component;
import service.finance.util.QueueDeclaration;

@Component
public class RabbitMQQueueConfig {

    private final AmqpAdmin amqpAdmin;

    public RabbitMQQueueConfig(AmqpAdmin amqpAdmin) {
        this.amqpAdmin = amqpAdmin;
    }

    public void setupQueues() {
        dlq();

        QueueDeclaration.queueMap()
                .forEach((key, destination) -> {
                    Exchange ex = ExchangeBuilder
                            .topicExchange(QueueDeclaration.PH_EXCHANGE)
                            .durable(Boolean.TRUE)
                            .build();
                    amqpAdmin.declareExchange(ex);

                    Queue q = QueueBuilder.durable(destination)
                            .deadLetterExchange(QueueDeclaration.PH_DEAD_LETTER_EXCHANGE)
                            .build();
                    amqpAdmin.declareQueue(q);

                    Binding b = BindingBuilder.bind(q)
                            .to(ex)
                            .with(key)
                            .noargs();
                    amqpAdmin.declareBinding(b);
                });
    }

    private void dlq() {

        FanoutExchange ex = ExchangeBuilder
                .fanoutExchange(QueueDeclaration.PH_DEAD_LETTER_EXCHANGE)
                .durable(Boolean.TRUE)
                .build();

        amqpAdmin.declareExchange(ex);

        Queue q = QueueBuilder
                .durable(QueueDeclaration.PH_DEAD_LETTER_QUEUE)
                .build();
        amqpAdmin.declareQueue(q);

        Binding b = BindingBuilder.bind(q).to(ex);
        amqpAdmin.declareBinding(b);
    }
}
