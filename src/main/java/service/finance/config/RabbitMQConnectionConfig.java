package service.finance.config;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.rabbitmq.RabbitFlux;
import reactor.rabbitmq.Receiver;
import reactor.rabbitmq.ReceiverOptions;
import reactor.rabbitmq.Sender;
import reactor.rabbitmq.SenderOptions;

@Configuration
public class RabbitMQConnectionConfig {

    final RabbitMQQueueConfig rabbitMQQueueConfig;

    public RabbitMQConnectionConfig(RabbitMQQueueConfig rabbitMQQueueConfig) {
        this.rabbitMQQueueConfig = rabbitMQQueueConfig;
    }

    @Bean
    Mono<Connection> connectionMono(RabbitProperties rabbitProperties) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(rabbitProperties.getHost());
        connectionFactory.setPort(rabbitProperties.getPort());
        connectionFactory.setUsername(rabbitProperties.getUsername());
        connectionFactory.setPassword(rabbitProperties.getPassword());
        connectionFactory.useNio();
        return Mono.fromCallable(() -> connectionFactory
                        .newConnection("finance"))
                .doOnSuccess(r ->rabbitMQQueueConfig.setupQueues()
                ).cache();
    }

    @Bean
    Sender sender(Mono<Connection> connectionMono) {
        return RabbitFlux
                .createSender(
                        new SenderOptions()
                                .connectionMono(connectionMono)
                                .resourceManagementScheduler(Schedulers.boundedElastic()));
    }

    @Bean
    Receiver receiver(Mono<Connection> connectionMono) {
        return RabbitFlux
                .createReceiver(
                    new ReceiverOptions()
                    .connectionMono(connectionMono)
                    .connectionSubscriptionScheduler(Schedulers.boundedElastic())
                    );
    }
}
