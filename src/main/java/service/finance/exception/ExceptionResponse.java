package service.finance.exception;

import java.util.List;

public record ExceptionResponse (List<String> errors){
}
