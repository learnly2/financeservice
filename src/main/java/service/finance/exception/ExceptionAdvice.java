package service.finance.exception;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Mono<ExceptionResponse>> error(NotFoundException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<Mono<ExceptionResponse>> handleException(WebExchangeBindException e) {
        var errors = e.getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        return ResponseEntity.badRequest().body(Mono.just(new ExceptionResponse(errors)));
    }

    @ExceptionHandler(DataViolationException.class)
    public ResponseEntity<Mono<ExceptionResponse>> error(DataViolationException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_ACCEPTABLE)
                .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
    }

    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<Mono<ExceptionResponse>> error(ConflictException e) {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
    }
}
