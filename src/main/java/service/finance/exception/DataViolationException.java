package service.finance.exception;

public class DataViolationException extends RuntimeException {
    public DataViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
