package service.finance.publisher;

import jakarta.annotation.PreDestroy;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.rabbitmq.OutboundMessage;
import reactor.rabbitmq.Sender;
import service.finance.message.InventoryQuantityMessage;
import service.finance.projection.BillItemProjection;
import service.finance.util.JacksonUtil;
import service.finance.util.QueueDeclaration;

import java.util.List;

@Component
public class RabbitMQPublisher {
    private final Sender sender;
    private final JacksonUtil jacksonUtil;

    public RabbitMQPublisher(
            Sender sender,
            JacksonUtil jacksonUtil
    ) {
        this.sender = sender;
        this.jacksonUtil = jacksonUtil;
    }

    Mono<Void> publishMessage(String exchange, String routingKey, String message) {
        return sender
                .sendWithPublishConfirms(Mono.just(new OutboundMessage(exchange, routingKey, message.getBytes())))
                //  .onErrorResume(e -> rabbitMQMessageLogService.save(message, false, true, routingKey).then(Mono.empty()))
                .then(Mono.empty());
    }

    public void sendQuantityMessage(List<BillItemProjection> items) {
        Mono.fromCallable(() -> jacksonUtil.serializeObject(new InventoryQuantityMessage(items)))
                .flatMap(m -> publishMessage(QueueDeclaration.PH_EXCHANGE, "QUANTITY_QUEUE_ROUTING_KEY", m))
                .subscribe();
    }

    @PreDestroy
    public void close() {
        sender.close();
    }
}