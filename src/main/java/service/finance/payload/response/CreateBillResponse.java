package service.finance.payload.response;

import service.finance.model.Bill;
import service.finance.model.BillItem;

import java.util.List;

public record CreateBillResponse(Bill bill, List<BillItem> items) {
}
