package service.finance.payload.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record ExemptBillItemRequest(
        @NotBlank(message = "Bill reference ID is required") String billId,
        @NotBlank(message = "Inventory item reference ID is required") String inventoryId
) {
}
