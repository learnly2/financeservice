package service.finance.payload.request;

import jakarta.validation.constraints.NotBlank;

public record CompleteBillRequest(
        @NotBlank(message = "Bill reference ID is required") String billId
) {
}
