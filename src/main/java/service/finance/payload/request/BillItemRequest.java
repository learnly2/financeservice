package service.finance.payload.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record BillItemRequest(
        @NotNull(message = "Quantity cannot be null") int quantity,
        @NotNull(message = "Price cannot be null") BigDecimal price,
        @NotBlank(message = "Inventory reference ID is required") String inventoryId
) {
}