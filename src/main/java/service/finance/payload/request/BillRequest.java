package service.finance.payload.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.List;

public record BillRequest(
        String billId,
        @NotNull(message = "Total price cannot be null") BigDecimal totalPrice,
        @NotBlank(message = "Customer reference ID is required") String customerId,
        @NotEmpty(message = "Item drop-off is required") @Valid List<BillItemRequest> items
) {
}
