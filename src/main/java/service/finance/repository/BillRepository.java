package service.finance.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import service.finance.model.Bill;
import service.finance.projection.BillProjection;

import static service.finance.util.Queries.FETCH_ALL_BILLS;

public interface BillRepository extends ReactiveCrudRepository<Bill, String> {
    Mono<Boolean> existsByIdAndCompleteTrue(String billId);

    @Query(FETCH_ALL_BILLS)
    Flux<BillProjection> getAllByOrderByCreatedAtDesc();
}
