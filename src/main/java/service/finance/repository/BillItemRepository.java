package service.finance.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import service.finance.model.BillItem;
import service.finance.projection.BillItemExtendedProjection;
import service.finance.projection.BillItemProjection;

import static service.finance.util.Queries.FETCH_ALL_BILL_ITEMS;

public interface BillItemRepository extends ReactiveCrudRepository<BillItem, String> {
    Mono<BillItem> getFirstByBillIdAndInventoryId(String billId, String inventoryId);

    Flux<BillItemProjection> getAllByBillIdAndExemptFalse(String billId);

    @Query(FETCH_ALL_BILL_ITEMS)
    Flux<BillItemExtendedProjection> getAllByBillId(@Param("billId") String billId);
}
