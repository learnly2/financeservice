package service.finance.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import service.finance.model.Bill;
import service.finance.projection.BillProjection;

import java.math.BigDecimal;

public interface BillService {
    Mono<Bill> save(String billId, BigDecimal totalPrice, String customerId);

    Mono<Bill> getOne(String billId);

    Mono<Bill> getOptional(String billId);

    Mono<Bill> completeBill(String billId);

    Mono<Boolean> checkIfComplete(String billId);

    Flux<BillProjection> getAllBills();

    Mono<Bill> reducePrice(String billId, BigDecimal value);
}
