package service.finance.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import service.finance.model.BillItem;
import service.finance.projection.BillItemExtendedProjection;
import service.finance.projection.BillItemProjection;

import java.math.BigDecimal;

public interface BillItemService {
    Mono<BillItem> save(int quantity, BigDecimal price, String inventoryId, String billId);

    Mono<BillItem> getOptional(String inventoryId, String billId);

    Mono<BillItem> exemptItem(String billId, String inventoryId);

    Mono<BillItem> getOne(String billItemId);

    Flux<BillItemProjection> getAllByBill(String billId);

    Flux<BillItemExtendedProjection> getAllBillItems(String billId);

    Mono<BillItem> getOne(String billId, String inventoryId);
}
