package service.finance.service;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import service.finance.exception.DataViolationException;
import service.finance.exception.NotFoundException;
import service.finance.model.Bill;
import service.finance.projection.BillProjection;
import service.finance.repository.BillRepository;

import java.math.BigDecimal;

@Service
public class BillServiceImpl implements BillService {

    final BillRepository repository;

    public BillServiceImpl(BillRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<Bill> save(String billId, BigDecimal totalPrice, String customerId) {
        return getOptional(billId)
                .map(b -> {
                    b.setCustomerId(customerId);
                    b.setTotalPrice(totalPrice);
                    return b;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public Mono<Bill> getOne(String billId) {
        return repository
                .findById(billId)
                .switchIfEmpty(Mono.error(new NotFoundException("NFE-001")));
    }

    @Override
    public Mono<Bill> getOptional(String billId) {
        return repository
                .findById(billId)
                .switchIfEmpty(Mono.just(new Bill()));
    }

    @Override
    public Mono<Bill> completeBill(String billId) {
        return getOne(billId)
                .map(b -> {
                    b.setComplete(true);
                    return b;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public Mono<Boolean> checkIfComplete(String billId) {
        return repository.existsByIdAndCompleteTrue(billId);
    }

    @Override
    public Flux<BillProjection> getAllBills() {
        return repository.getAllByOrderByCreatedAtDesc();
    }

    @Override
    public synchronized Mono<Bill> reducePrice(String billId, BigDecimal value) {
        return getOne(billId)
                .map(b -> {
                    b.setTotalPrice(b.getTotalPrice().subtract(value));
                    return b;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }
}
