package service.finance.service;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import service.finance.exception.DataViolationException;
import service.finance.exception.NotFoundException;
import service.finance.model.BillItem;
import service.finance.projection.BillItemExtendedProjection;
import service.finance.projection.BillItemProjection;
import service.finance.repository.BillItemRepository;

import java.math.BigDecimal;

@Service
public final class BillItemServiceImpl implements BillItemService {

    final BillItemRepository repository;

    public BillItemServiceImpl(BillItemRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<BillItem> save(int quantity, BigDecimal price, String inventoryId, String billId) {
        return getOptional(inventoryId, billId)
                .map(bi -> {
                    bi.setBillId(billId);
                    bi.setPrice(price);
                    bi.setQuantity(quantity);
                    bi.setInventoryId(inventoryId);
                    return bi;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public Mono<BillItem> getOptional(String inventoryId, String billId) {
        return repository
                .getFirstByBillIdAndInventoryId(billId, inventoryId)
                .switchIfEmpty(Mono.just(new BillItem()));
    }

    @Override
    public Mono<BillItem> exemptItem(String billId, String inventoryId) {
        return getOne(billId, inventoryId)
                .map(bi -> {
                    bi.setExempt(true);
                    return bi;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public Mono<BillItem> getOne(String billItemId) {
        return repository.findById(billItemId)
                .switchIfEmpty(Mono.error(new NotFoundException("NFE-001")));
    }

    @Override
    public Flux<BillItemProjection> getAllByBill(String billId) {
        return repository.getAllByBillIdAndExemptFalse(billId);
    }

    @Override
    public Flux<BillItemExtendedProjection> getAllBillItems(String billId) {
        return repository.getAllByBillId(billId);
    }

    @Override
    public Mono<BillItem> getOne(String billId, String inventoryId) {
        return repository.getFirstByBillIdAndInventoryId(billId, inventoryId);
    }
}
