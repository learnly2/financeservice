CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

SELECT uuid_generate_v1mc();

CREATE SCHEMA IF NOT EXISTS inventory;
CREATE SCHEMA IF NOT EXISTS profile;

CREATE TABLE IF NOT EXISTS bill
(
    id         varchar(100) not null primary key DEFAULT uuid_generate_v1mc(),
    complete   boolean      not null,
    totalPrice NUMERIC      not null,
    customerId varchar(200) not null,
    createdAt  timestamp    not null,
    updatedAt  timestamp    not null
);


CREATE TABLE IF NOT EXISTS billitem
(
    id          varchar(100)   not null primary key DEFAULT uuid_generate_v1mc(),
    quantity    NUMERIC(10, 0) not null,
    price       NUMERIC        not null,
    exempt      boolean        not null,
    billId      varchar(200)   not null,
    inventoryId varchar(200)   not null,
    createdAt   timestamp      not null,
    updatedAt   timestamp      not null,
    constraint fk_billId foreign key (billId) references bill (id)
);

CREATE TABLE IF NOT EXISTS profile.customer
(
    id          varchar(100) not null primary key DEFAULT uuid_generate_v1mc(),
    fullName    varchar      not null,
    phoneNumber varchar(30) UNIQUE,
    createdBy   varchar(100) not null,
    createdAt   timestamp    not null,
    updatedAt   timestamp    not null
);

CREATE TABLE IF NOT EXISTS inventory.inventory
(
    id        varchar(100)   not null primary key DEFAULT uuid_generate_v1mc(),
    name      varchar(200)   not null,
    quantity  NUMERIC(10, 0) not null,
    price     NUMERIC        not null,
    exempt    boolean        not null,
    createdAt timestamp      not null,
    updatedAt timestamp      not null
)
