package service.finance.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import service.finance.model.BillItem;
import service.finance.projection.BillItemProjection;
import service.finance.repository.BillItemRepository;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class BillItemServiceImplTest {

    @Mock
    BillItemRepository repository;

    @InjectMocks
    BillItemServiceImpl billItemService;

    BillItem mockBillItem;

    @BeforeEach
    void setUp() {
        billItemService = new BillItemServiceImpl(repository);
        mockBillItem = new BillItem();
        mockBillItem.setBillId("bill_1");
        mockBillItem.setInventoryId("inventory_1");
        mockBillItem.setId("billItem_1");
        mockBillItem.setQuantity(5);
        mockBillItem.setPrice(BigDecimal.valueOf(300));
    }

    @AfterEach
    void tearDown() {
        mockBillItem = null;
    }

    @Test
    void saveTest() {
        Mockito.when(repository.getFirstByBillIdAndInventoryId("bill_1", "inventory_1"))
                .thenReturn(Mono.just(mockBillItem));

        Mockito.when(repository.save(mockBillItem)).thenReturn(Mono.just(mockBillItem));

        StepVerifier
                .create(billItemService.save(10, BigDecimal.valueOf(600), "inventory_1", "bill_1"))
                .assertNext(billItem -> {
                    assertEquals(10, billItem.getQuantity());
                    assertEquals(BigDecimal.valueOf(600), billItem.getPrice());
                })
                .expectComplete()
                .verify();

        Mockito.verify(repository).getFirstByBillIdAndInventoryId("bill_1", "inventory_1");
        Mockito.verify(repository).save(mockBillItem);
    }

    @Test
    void getOptionalTest() {
        Mockito.when(repository.getFirstByBillIdAndInventoryId("bill_1", "inventory_1"))
                .thenReturn(Mono.just(mockBillItem));

        StepVerifier
                .create(billItemService.getOptional("inventory_1", "bill_1"))
                .expectSubscription()
                .expectNextCount(1)
                .verifyComplete();

        Mockito.verify(repository).getFirstByBillIdAndInventoryId("bill_1", "inventory_1");

        Mockito.when(repository.getFirstByBillIdAndInventoryId("bill_2", "inventory_2"))
                .thenReturn(Mono.empty());
        BillItem item = new BillItem();
        StepVerifier
                .create(billItemService.getOptional("inventory_2", "bill_2"))
                .expectSubscription()
                .assertNext(billItem -> {
                    assertEquals(item.getPrice(), billItem.getPrice());
                    assertEquals(item.getId(), billItem.getId());
                    assertEquals(item.getQuantity(), billItem.getQuantity());
                    assertEquals(item.getInventoryId(), billItem.getInventoryId());
                    assertEquals(item.getBillId(), billItem.getBillId());
                })
                .verifyComplete();

        Mockito.verify(repository).getFirstByBillIdAndInventoryId("bill_2", "inventory_2");
    }

    @Test
    void exemptItemTest() {
        Mockito.when(repository.getFirstByBillIdAndInventoryId("bill_1", "inventory_1"))
                .thenReturn(Mono.just(mockBillItem));
        Mockito.when(repository.save(mockBillItem)).thenReturn(Mono.just(mockBillItem));

        StepVerifier
                .create(billItemService.exemptItem("bill_1", "inventory_1"))
                .expectSubscription()
                .assertNext(billItem -> assertTrue(billItem.isExempt()))
                .verifyComplete();
        Mockito.verify(repository).getFirstByBillIdAndInventoryId("bill_1", "inventory_1");
        Mockito.verify(repository).save(mockBillItem);
    }

    @Test
    void getOneTest() {

    }

    @Test
    void getAllByBillTest() {
        Flux<BillItemProjection> billItemProjectionFlux = Flux
                .just(new BillItemProjection("inventory_2", 10, "bill_3", false));

        Mockito.when(repository.getAllByBillIdAndExemptFalse(Mockito.eq("bill_3")))
                .thenReturn(billItemProjectionFlux);

        StepVerifier
                .create(billItemService.getAllByBill("bill_3"))
                .expectSubscription()
                .expectNextCount(1)
                .verifyComplete();

        Mockito.when(repository.getAllByBillIdAndExemptFalse(Mockito.eq("bill_4")))
                .thenReturn(Flux.empty());
        StepVerifier
                .create(billItemService.getAllByBill("bill_4"))
                .expectSubscription()
                .expectNextCount(0)
                .verifyComplete();
    }
}