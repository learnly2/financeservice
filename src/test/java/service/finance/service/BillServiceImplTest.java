package service.finance.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import service.finance.exception.NotFoundException;
import service.finance.model.Bill;
import service.finance.repository.BillRepository;
import java.math.BigDecimal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class BillServiceImplTest {

    @Mock
    BillRepository repository;

    @InjectMocks
    BillServiceImpl billService;

    Bill mockBill;

    @BeforeEach
    void setUp() {
        billService = new BillServiceImpl(repository);
        mockBill = new Bill();

        mockBill.setId("bill_1");
        mockBill.setCustomerId("customer_1");
        mockBill.setTotalPrice(BigDecimal.valueOf(200));
    }

    @AfterEach
    void tearDown() {
        billService = null;
    }

    @Test
    void saveTest() {
        Mockito.when(repository.findById("bill_1")).thenReturn(Mono.just(mockBill));
        Mockito.when(repository.save(mockBill)).thenReturn(Mono.just(mockBill));

        // Act
        StepVerifier.create(billService.save("bill_1", BigDecimal.valueOf(200), "customer_1"))
                .expectNext(mockBill)
                .expectComplete()
                .verify();

        // Assert
        Mockito.verify(repository).save(mockBill);
    }

    @Test
    void getOneTest() {
        Mockito.when(repository.findById("bill_1")).thenReturn(Mono.just(mockBill));
        StepVerifier
                .create(billService.getOne("bill_1"))
                .expectNext(mockBill)
                .verifyComplete();

        Mockito.when(repository.findById("bill_2")).thenReturn(Mono.empty());
        StepVerifier
                .create(billService.getOne("bill_2"))
                .expectError(NotFoundException.class)
                .verify();

        Mockito.verify(repository).findById("bill_1");
        Mockito.verify(repository).findById("bill_2");
    }

    @Test
    void getOptionalTest() {
        String billId = "bill_2";
        Mockito.when(repository.findById(Mockito.eq(billId))).thenReturn(Mono.empty());
        Bill expectedBill = new Bill();

        StepVerifier
                .create(billService.getOptional(billId))
                .assertNext(bill -> {
                    assertEquals(bill.getId(), expectedBill.getId());
                    assertEquals(bill.getCustomerId(), expectedBill.getCustomerId());
                    assertEquals(bill.getTotalPrice(), expectedBill.getTotalPrice());
                    assertEquals(bill.isComplete(), expectedBill.isComplete());
                })
                .expectComplete()
                .verify();

        Mockito.verify(repository).findById("bill_2");
    }

    @Test
    void completeBillTest() {
        Mockito.when(repository.findById(Mockito.eq("bill_1"))).thenReturn(Mono.just(mockBill));
        Mockito.when(repository.save(mockBill)).thenReturn(Mono.just(mockBill));

        StepVerifier
                .create(billService.completeBill("bill_1"))
                .assertNext(bill -> assertTrue(bill.isComplete()))
                .verifyComplete();

        Mockito.verify(repository).save(mockBill);
        Mockito.verify(repository).findById("bill_1");
    }

    @Test
    void checkIfComplete() {
        Mockito.when(repository.existsByIdAndCompleteTrue(Mockito.eq("bill_1"))).thenReturn(Mono.just(false));
        StepVerifier
                .create(billService.checkIfComplete("bill_1"))
                .assertNext(Assertions::assertFalse)
                .expectComplete()
                .verify();

        Mockito.verify(repository).existsByIdAndCompleteTrue("bill_1");
    }

    @Test
    void reducePrice() {
        Mockito.when(repository.findById(Mockito.eq("bill_1"))).thenReturn(Mono.just(mockBill));
        Mockito.when(repository.save(mockBill)).thenReturn(Mono.just(mockBill));

        StepVerifier
                .create(billService.reducePrice("bill_1", BigDecimal.valueOf(100)))
                .assertNext(bill -> assertEquals(BigDecimal.valueOf(100), bill.getTotalPrice()))
                .expectComplete()
                .verify();

        Mockito.verify(repository).save(mockBill);
        Mockito.verify(repository).findById("bill_1");
    }
}