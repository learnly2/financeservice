package service.finance.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import service.finance.projection.BillProjection;

import static org.junit.jupiter.api.Assertions.*;

class BillRepositoryTest extends BaseRepository {

    @Autowired
    private BillRepository billRepository;

    @Test
    void existsByIdAndCompleteTrue() {
        String billId = "billId";
        Mono<Boolean> existsMono = billRepository.existsByIdAndCompleteTrue(billId);
        assertNotNull(existsMono);
        StepVerifier
                .create(existsMono)
                .assertNext(Assertions::assertFalse)
                .verifyComplete();
    }

    @Test
    void getAllByOrderByCreatedAtDesc() {
        Flux<BillProjection> billsFlux = billRepository.getAllByOrderByCreatedAtDesc();
        assertNotNull(billsFlux);

        StepVerifier.create(billsFlux)
                .expectNextCount(0)
                .expectComplete()
                .verify();
    }
}