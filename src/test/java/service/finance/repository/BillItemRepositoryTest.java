package service.finance.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import service.finance.model.BillItem;
import service.finance.projection.BillItemExtendedProjection;
import service.finance.projection.BillItemProjection;

import static org.junit.jupiter.api.Assertions.*;

class BillItemRepositoryTest extends BaseRepository {

    @Autowired
    BillItemRepository repository;

    @Test
    void testGetFirstByBillIdAndInventoryId() {
        String billId = "bill_1";
        String inventoryId = "inventory_1";
        Mono<BillItem> itemMono = repository.getFirstByBillIdAndInventoryId(billId, inventoryId);

        StepVerifier
                .create(itemMono)
                .expectSubscription()
                .expectNextCount(0)
                .expectComplete()
                .verify();
    }

    @Test
    void testGetAllByBillIdAndExemptFalse() {
        String billId = "bill_1";
        Flux<BillItemProjection> billItemFlux = repository.getAllByBillIdAndExemptFalse(billId);

        StepVerifier
                .create(billItemFlux)
                .expectSubscription()
                .expectNextCount(0)
                .expectComplete()
                .verify();
    }

    @Test
    void testGetAllByBillId() {
        String billId = "bill_1";
        Flux<BillItemExtendedProjection> projectionFlux =  repository.getAllByBillId(billId);

        StepVerifier
                .create(projectionFlux)
                .expectSubscription()
                .expectNextCount(0)
                .expectComplete()
                .verify();
    }
}